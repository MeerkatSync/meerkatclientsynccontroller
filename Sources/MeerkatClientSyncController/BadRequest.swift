//
//  BadRequest.swift
//  
//
//  Created by Filip Klembara on 11/03/2020.
//

public struct SendError {
    public enum Action {
        case pull
        case push
        case createGroup(id: GroupID)
        case removeGroup(id: GroupID)
        case subscribe(userId: UserID, as: Role, for: GroupID)
        case unsubscribe(userId: UserID, from: GroupID)
    }
    public let action: Action
    public let error: Error
}

public struct BadRequest: Error {
    public let code: Int
    public let body: String

    public init(code: Int, body: String? = nil) {
        self.code = code
        if let body = body {
            self.body = body
        } else {
            self.body = ""
        }
    }
}
