//
//  ClientSyncController.swift
//  
//
//  Created by Filip Klembara on 28/02/2020.
//

import Foundation

public final class SyncController<DB: ClientDBWrapper>: ClientSyncController, Cancellable {
    private typealias Sender = Publishers.MeerkatSender<DB.Upstream, AuthConfigPublisher>

    private typealias AuthConfigPublisher = PassthroughSubject<SyncConfig?, Never>

    private let clientConfig: ClientConfig
    private var syncConfig: SyncConfig?
    private let mutex = DispatchSemaphore(value: 1)

    private enum State {
        case sync(cancellable: Cancellable, errorCancellable: Cancellable)
        case dropped
    }

    private var state: State = .dropped

    private let authPublisher: AuthConfigPublisher = .init()
    private let netErrorPublisher: PassthroughSubject<SendError, Never> = .init()

    private let db: DB

    public init(db: DB, clientConfig: ClientConfig, syncConfig: SyncConfig? = nil) {
        self.db = db
        self.clientConfig = clientConfig
        self.syncConfig = syncConfig
        guard let conf = syncConfig else { return }
        start(syncConfig: conf)
    }

    private func setUpSyncConfig(_ syncConfig: SyncConfig?) {
        mutex.wait()
        self.syncConfig = syncConfig
        if case .dropped = state {
            mutex.signal()
            setUpSynchronization()
        } else {
            mutex.signal()
        }
        authPublisher.send(syncConfig)
    }

    private func setUpSynchronization() {
        mutex.wait()
        defer { mutex.signal() }
        guard case .dropped = state else {
            return
        }
        let dbPublisher = db.dbPublisher()
        let sender = dbPublisher.syncController(config: clientConfig, syncConfig: authPublisher)
        let ec = sender.networkErrors.sink { [weak self] (err) in
            self?.netErrorPublisher.send(err)
        }
        let subs = db.dbSubscriber()
        sender.subscribe(subs)
        state = .sync(cancellable: subs, errorCancellable: ec)
    }

    public func dropSynchronizationAndDatabase() throws {
        mutex.wait()
        defer { mutex.signal() }
        lockedCancel()
        try db.dropSyncObjects()
    }

    public var networkErrorPublisher: AnyPublisher<SendError, Never> {
        netErrorPublisher.eraseToAnyPublisher()
    }

    public func stop() {
        setUpSyncConfig(nil)
    }

    public func start(syncConfig: SyncConfig) {
        setUpSyncConfig(syncConfig)
    }

    public func forceRefresh() {
        mutex.wait()
        guard let conf = syncConfig else {
            mutex.signal()
            return
        }
        mutex.signal()
        stop()
        start(syncConfig: conf)
    }

    private func lockedCancel() {
        guard case let .sync(sub, errSub) = state else {
            return
        }
        errSub.cancel()
        sub.cancel()
        state = .dropped
    }
    public func cancel() {
        mutex.wait()
        defer { mutex.signal() }
        lockedCancel()
    }

    deinit {
        cancel()
    }
}
