//
//  ClientConfig.swift
//  
//
//  Created by Filip Klembara on 19/02/2020.
//

import Foundation

public enum HTTPScheme: String {
    case http = "http"
    case https = "https"
}

public struct SyncConfig {
    public let authToken: String
    public let syncToken: String

    public init(authToken: String, syncToken: String) {
        self.authToken = authToken
        self.syncToken = syncToken
    }
}

public struct ClientConfig {
    public let scheme: HTTPScheme
    public let host: String
    public let port: Int
    public let rootUrl: String

    public init(scheme: HTTPScheme = .https, host: String, port: Int = 443, rootUrl: String = "/") {
        self.scheme = scheme
        self.host = host
        self.port = port
        var root = rootUrl
        if root.first == "/" {
            root.removeFirst()
        }
        self.rootUrl = root
    }

    public var url: URL {
        URL(string: "\(scheme.rawValue)://\(host):\(port)/\(rootUrl)")!
    }

    public func urlAppendingPathComponent(_ componenet: String) -> URL {
        var u = url
        u.appendPathComponent(componenet)
        return u
    }
}
