//
//  MeerkatSender.swift
//  
//
//  Created by Filip Klembara on 19/02/2020.
//

import Foundation
import Network

extension Publisher where Output == UpstreamMessage {
    public func syncController<SyncConfigPublisher: Publisher>(config: ClientConfig, syncConfig: SyncConfigPublisher, maxRetries: Int = 3) -> Publishers.MeerkatSender<Self, SyncConfigPublisher> where SyncConfigPublisher.Output == SyncConfig?, SyncConfigPublisher.Failure == Never {
        assert(maxRetries >= 0)
        return Publishers.MeerkatSender(upstream: self, syncConfig: syncConfig, config: config, maxRetries: maxRetries)
    }
}

extension Publishers {
    public final class MeerkatSender<Upstream: Publisher, SyncConfigUpstream: Publisher>: Publisher
    where Upstream.Output == UpstreamMessage, SyncConfigUpstream.Output == SyncConfig?, SyncConfigUpstream.Failure == Never {
        private let upstream: Upstream
        private let config: ClientConfig
        public let networkErrors = PassthroughSubject<SendError, Never>()
        private let maxRetries: Int
        private let syncConfigUpstream: SyncConfigUpstream
        public init(upstream: Upstream, syncConfig: SyncConfigUpstream, config: ClientConfig, maxRetries: Int) {
            self.upstream = upstream
            self.config = config
            self.maxRetries = maxRetries
            self.syncConfigUpstream = syncConfig
        }

        public func receive<S>(subscriber: S) where S : Subscriber, MeerkatSender.Failure == S.Failure, MeerkatSender.Output == S.Input {
            upstream.subscribe(Inner(downstream: subscriber, syncConfig: syncConfigUpstream, config: config, networkErrors: networkErrors, maxRetries: maxRetries))
        }

        public typealias Output = DownstreamMessage

        public typealias Failure = Upstream.Failure
    }
}

extension Publishers.MeerkatSender {
    fileprivate final class Inner<Downstream: Subscriber, SyncConfigUpstream: Publisher> where Downstream.Input == DownstreamMessage, SyncConfigUpstream.Output == SyncConfig?, SyncConfigUpstream.Failure == Never {
        private var status = SubscriptionStatus.awaitingSubscription
        private let downstream: Downstream
        private let mutex = DispatchSemaphore(value: 1)
        private let queue = DispatchQueue(label: "Monitor")
        private let monitor = NWPathMonitor()
        private var requestedDemand = false
        private let networkErrors: PassthroughSubject<SendError, Never>
        private var currentTransaction: Input? {
            didSet {
                guard case .subscribed = status else { return }
                guard let sc = syncConfig, let tr = currentTransaction else {
                    task?.cancel()
                    task = nil
                    return
                }
                let trans = setGroup(in: tr, syncConfig: sc)
                let url = config.urlAppendingPathComponent(trans.url(clientSyncId: sc.syncToken))
                var request = URLRequest(url: url)
                request.httpMethod = trans.method
                request.httpBody = trans.body
                request.setBearer(token: sc.authToken)
                request.setJSONBodyType()
                lockedSendRequest(request)
            }
        }

        private var task: Cancellable?
        private var retries = 0
        private var maxRetries = 3
        private var config: ClientConfig
        private var isConnected = false {
            didSet {
                guard isConnected != oldValue else {
                    return
                }
                assert(mutex.wait(timeout: .now()) == .timedOut)
                guard case .subscribed = status else {
                    monitor.cancel()
                    return
                }
                if isConnected {
                    if let tmp = currentTransaction, task == nil {
                        currentTransaction = tmp
                    }
                } else {
                    task?.cancel()
                    task = nil
                    if case let .pull(req, _) = currentTransaction {
                        currentTransaction = nil
                        requestedDemand = false
                        mutex.signal()
                        req.complete(.failed, by: .meerkat)
                        mutex.wait()
                    }
                }
            }
        }
        private var _currentTransaction: Input? = nil
        private var syncConfig: SyncConfig? = nil {
            didSet {
                mutex.wait()
                defer { mutex.signal() }
                guard syncConfig != nil else {
                    if case let .pull(req, _) = currentTransaction {
                        currentTransaction = nil
                        requestedDemand = false
                        mutex.signal()
                        req.complete(.failed, by: .meerkat)
                        mutex.wait()
                    }
                    retries = maxRetries
                    _currentTransaction = currentTransaction
                    currentTransaction = nil
                    return
                }
                retries = 0
                if let trans = currentTransaction {
                    _currentTransaction = trans
                }
                currentTransaction = _currentTransaction
                _currentTransaction = nil
            }
        }
        private var syncConfigUpstream: AnyCancellable!
        private var demands: Subscribers.Demand = .none
        fileprivate init(downstream: Downstream, syncConfig: SyncConfigUpstream, config: ClientConfig, networkErrors: PassthroughSubject<SendError, Never>, maxRetries: Int) {
            assert(maxRetries >= 0)
            self.maxRetries = maxRetries
            self.networkErrors = networkErrors
            self.config = config
            self.downstream = downstream
            monitor.pathUpdateHandler = { [weak self] path in
                self?.mutex.wait()
                defer { self?.mutex.signal() }
                self?.isConnected = path.status == .satisfied
            }
            startMonitor()
            syncConfigUpstream = syncConfig.assign(to: \.syncConfig, on: self)
        }

        private func startMonitor() {
            monitor.start(queue: queue)
        }
        private func stopMonitor() {
            monitor.cancel()
        }
        deinit {
            stopMonitor()
        }
    }
}

extension Publishers.MeerkatSender.Inner: Subscription {
    func request(_ demand: Subscribers.Demand) {
        mutex.wait()
        guard case let .subscribed(subscription) = status else {
            mutex.signal()
            return
        }
        demands += demand
        guard currentTransaction == nil else {
            mutex.signal()
            return
        }
        guard demands > .none && !requestedDemand else {
            mutex.signal()
            return
        }
        requestedDemand = true
        mutex.signal()
        subscription.request(.max(1))
    }

    func cancel() {
        mutex.wait()
        stopMonitor()
        demands = .none
        let prevTransaction = currentTransaction
        currentTransaction = nil
        requestedDemand = true
        task?.cancel()
        task = nil
        syncConfigUpstream?.cancel()
        syncConfigUpstream = nil
        guard case let .subscribed(subscription) = status else {
            mutex.signal()
            return
        }
        status = .terminated
        mutex.signal()
        if case let .pull(req, _) = prevTransaction {
            mutex.signal()
            req.complete(.failed, by: .meerkat)
            mutex.wait()
        }
        subscription.cancel()
    }
}

extension Publishers.MeerkatSender.Inner: Subscriber {
    func receive(subscription: Subscription) {
        mutex.wait()
        guard case .awaitingSubscription = status else {
            mutex.signal()
            subscription.cancel()
            return
        }
        status = .subscribed(subscription: subscription)
        mutex.signal()
        downstream.receive(subscription: self)
    }

    func receive(_ input: Input) -> Subscribers.Demand {
        mutex.wait()
        defer { mutex.signal() }
        guard case .subscribed = status else { return .none }
        assert(currentTransaction == nil)
        assert(requestedDemand)
        if case let .pull(req, _) = input, !isConnected {
            mutex.signal()
            req.complete(.failed, by: .meerkat)
            mutex.wait()
            return .max(1)
        }
        retries = 0
        currentTransaction = input
        return .none
    }

    func receive(completion: Subscribers.Completion<Downstream.Failure>) {
        guard case .subscribed = status else { return }
        cancel()
        networkErrors.send(completion: .finished)
        downstream.receive(completion: completion)
    }

    private func setGroup(in input: Input, syncConfig: SyncConfig) -> Input {
        switch input {
        case .push(let p):
            return .push(p.changeFromDefaultGroup(to: syncConfig.syncToken))
        case .group(let logId, let message):
            switch message {
            case .remove(let groupId):
                guard groupId == SyncGroupDefaults.defaultName else { return input }
                return .group(logId: logId, message: .remove(groupId: syncConfig.syncToken))
            case .add(let groupId):
                guard groupId == SyncGroupDefaults.defaultName else { return input }
                return .group(logId: logId, message: .add(groupId: syncConfig.syncToken))
            default:
                return input
            }
        case .pull(let request, let pull):
            return .pull(request: request, pull: pull.changeFromDefaultGroup(to: syncConfig.syncToken))
        }
    }

    private func setGroup(in output: DownstreamMessage, syncToken: String) -> DownstreamMessage {
        switch output {
        case .diff(let d, let req):
            return .diff(d.changeToDefaultGroup(from: syncToken), pullRequest: req)
        case .groupRemoved(let logId, let groupId):
            guard groupId == syncToken else { return output }
            return .groupRemoved(logId: logId, groupId: SyncGroupDefaults.defaultName)
        case .groupCreated(let logId, let groupId):
            guard groupId == syncToken else { return output }
            return .groupCreated(logId: logId, groupId: SyncGroupDefaults.defaultName)
        case .failedGroupCreate(let logId, let groupId):
            if groupId == syncToken {
                return .groupCreated(logId: logId, groupId: SyncGroupDefaults.defaultName)
            }
            return output
        default:
            return output
        }
    }

    typealias Input = UpstreamMessage

    private func handleCompletion(completion: Subscribers.Completion<Error>, syncToken: String) {
        mutex.wait()
        guard case .subscribed = status else { mutex.signal(); return }
        func nextTransaction() {
            assert(requestedDemand)
            assert(currentTransaction != nil)
            currentTransaction = nil
            requestedDemand = false
            mutex.signal()
            request(.none)
        }
        switch completion {
        case .finished:
            nextTransaction()
        case .failure(let err):
            if case let .pull(req, _) = currentTransaction {
                mutex.signal()
                req.complete(.failed, by: .meerkat)
                mutex.wait()
                nextTransaction()
                mutex.signal()
                networkErrors.send(.init(action: .pull, error: err))
                mutex.wait()
                return
            }
            if let error = err as? BadRequest {
                if [403, 404, 409].contains(error.code) {
                    lockedDropTransaction(syncToken: syncToken)
                    let err = currentTransaction!.asSendErrorAction
                    nextTransaction()
                    networkErrors.send(.init(action: err, error: error))
                    return
                }
            }
            if retries >= maxRetries {
                mutex.signal()
                networkErrors.send(.init(action: currentTransaction!.asSendErrorAction, error: err))
                mutex.wait()
            }
            defer { mutex.signal() }
            retries += 1
            let timeOffset: Double = retries < maxRetries ? 2 : (retries < maxRetries * 3 ? 10 : 60)
            DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + timeOffset) { [weak self] in
                guard let s = self else {
                    return
                }
                s.mutex.wait()
                defer { s.mutex.signal() }
                guard let t = s.currentTransaction else {
                    return
                }
                s.currentTransaction = t
            }
        }
    }

    private func lockedDropTransaction(syncToken: String) {
        guard let trans = currentTransaction else {
            return
        }
        guard case let .group(logId, message) = trans else {
            return
        }
        mutex.signal()
        defer { mutex.wait() }
        switch message {
        case .remove(let groupId):
            handleReceive(log: .groupRemoved(logId: logId, groupId: groupId), syncToken: syncToken)
        case .add(let groupId):
            handleReceive(log: .failedGroupCreate(logId: logId, groupId: groupId), syncToken: syncToken)
        case .subscribe(let userId, _, let groupId):
            handleReceive(log: .failedSubscribe(logId: logId, userId: userId, groupId: groupId), syncToken: syncToken)
        case .unsubscribe(let userId, let groupId):
            handleReceive(log: .unsubscribed(logId: logId, userId: userId, groupId: groupId), syncToken: syncToken)
        }
    }

    private func handleReceive(log: DownstreamMessage, syncToken: String) {
        mutex.wait()
        guard case .subscribed = status else { mutex.signal(); return }
        guard self.syncConfig == nil || self.syncConfig?.syncToken == syncToken else {
            mutex.signal()
            if case let .pull(req, _) = currentTransaction {
                req.complete(.noData, by: .meerkat)
            }
            return
        }
        let log = setGroup(in: log, syncToken: syncToken)
        mutex.signal()
        let d = downstream.receive(log)
        mutex.wait()
        demands -= 1
        mutex.signal()
        request(d)
    }

    private func lockedSendRequest(_ request: URLRequest) {
        guard case .subscribed = status else { return }
        task?.cancel()
        assert(syncConfig != nil)
        guard isConnected, let trans = currentTransaction, let sc = syncConfig else {
            if case let .pull(req, _) = currentTransaction {
                mutex.signal()
                req.complete(.noData, by: .meerkat)
                mutex.wait()
                currentTransaction = nil
                requestedDemand = false
                mutex.signal()
                self.request(.none)
                mutex.wait()
            }
            task = nil
            return
        }
        let tmp = URLSession.shared.dataTaskPublisher(for: request)
            .tryMap { output -> Data in
                guard let response = output.response as? HTTPURLResponse else {
                    throw URLError(URLError.Code.badServerResponse)
                }
                guard response.statusCode == trans.expectingCode else {
                    throw BadRequest(code: response.statusCode, body: String(data: output.data, encoding: .utf8) ?? "Unable to encode body from response")
                }
                return output.data
        }
        let decodedTmp: AnyPublisher<Downstream.Input, Error>
        switch trans {
        case .push(let push):
            let logId = push.syncLogId
            let decod = JSONDecoder()
            decod.dateDecodingStrategy = .iso8601
            decodedTmp = tmp.decode(type: CodableDiff.self, decoder: decod)
                .map { diff -> DownstreamMessage in
                    .diff(LogCodableDiff(syncLogId: logId, diff: diff), pullRequest: nil)
            }.eraseToAnyPublisher()
        case .group(let logId, let message):
            switch message {
            case .remove(let groupId):
                decodedTmp = tmp.map { _ in DownstreamMessage.groupRemoved(logId: logId, groupId: groupId) }.eraseToAnyPublisher()
            case .add(let groupId):
                decodedTmp = tmp.map { _ in DownstreamMessage.groupCreated(logId: logId, groupId: groupId) }.eraseToAnyPublisher()
            case .subscribe(let userId, _, let groupId):
                decodedTmp = tmp.map { _ in DownstreamMessage.subscribed(logId: logId, userId: userId, groupId: groupId) }.eraseToAnyPublisher()
            case .unsubscribe(let userId, let groupId):
            decodedTmp = tmp.map { _ in DownstreamMessage.unsubscribed(logId: logId, userId: userId, groupId: groupId) }.eraseToAnyPublisher()
            }
        case .pull(let request, _):
            let logId = "<UNKNOWN_LOG_ID>"
            let decod = JSONDecoder()
            decod.dateDecodingStrategy = .iso8601
            decodedTmp = tmp.decode(type: CodableDiff.self, decoder: decod)
                .map { diff -> DownstreamMessage in
                    .diff(LogCodableDiff(syncLogId: logId, diff: diff), pullRequest: request)
            }.eraseToAnyPublisher()
        }
        task = decodedTmp.sink(receiveCompletion: {
            self.handleCompletion(completion: $0, syncToken: sc.syncToken)
        }) {
            self.handleReceive(log: $0, syncToken: sc.syncToken)
        }
    }
}

extension UpstreamMessage {
    var method: String {
        switch self {
        case .push:
            return "PATCH"
        case .group(_ , let message):
            switch message {
                case .remove:
                    return "DELETE"
                case .add:
                    return "POST"
                case .subscribe:
                    return "POST"
                case .unsubscribe:
                    return "DELETE"
            }
        case .pull:
            return "POST"
        }
    }

    func url(clientSyncId: String) -> String {
        switch self {
        case .push:
            return "sync/\(clientSyncId)"
        case .pull:
            return "sync/\(clientSyncId)"
        case .group(_ , let message):
            switch message {
            case .remove(let groupId):
                return "groups/\(groupId)"
            case .add(let groupId):
                return "groups/\(groupId)"
            case .subscribe(let userId, _, let groupId):
                return "groups/\(groupId)/subscribers/\(userId)"
            case .unsubscribe(let userId, let groupId):
                return "groups/\(groupId)/subscribers/\(userId)"
            }
        }
    }

    var body: Data? {
        switch self {
        case .push(let push):
            return try! JSONEncoder().encode(push.push)
        case .group(_ , let message):
            switch message {
                case .remove:
                    return nil
                case .add:
                    return nil
                case .subscribe(_, let role, _):
                    return try! JSONEncoder().encode(CodableSubscribeBody(role: role))
                case .unsubscribe:
                    return nil
            }
        case .pull(_, let pull):
            return try! JSONEncoder().encode(pull)
        }
    }

    var expectingCode: Int {
        switch self {
        case .push:
            return 200
        case .pull:
            return 200
        case .group(_ , let message):
            switch message {
            case .remove:
                return 200
            case .add:
                return 201
            case .subscribe:
                return 201
            case .unsubscribe:
                return 200
            }
        }
    }

    var asSendErrorAction: SendError.Action {
        switch self {
        case .push:
            return .push
        case .group(_, let message):
            switch message {
            case .remove(groupId: let groupId):
                return .removeGroup(id: groupId)
            case .add(let groupId):
                return .createGroup(id: groupId)
            case .subscribe(let userId, let role, let groupId):
                return .subscribe(userId: userId, as: role, for: groupId)
            case .unsubscribe(let userId, let groupId):
                return .unsubscribe(userId: userId, from: groupId)
            }
        case .pull:
            return .pull
        }
    }
}
