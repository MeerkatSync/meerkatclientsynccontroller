//
//  MeerkatClientAPI.swift
//  
//
//  Created by Filip Klembara on 24/04/2020.
//

import Foundation

open class MeerkatClientAPI: ObservableObject {

    private var _loadingCounter: Int = 0

    private func incLoading() {
        DispatchQueue.main.async {
            if self._loadingCounter == 0 {
                self.objectWillChange.send()
            }
            self._loadingCounter += 1
        }
    }

    private func decLoading() {
        DispatchQueue.main.async {
            if self._loadingCounter == 1 {
                self.objectWillChange.send()
            }
            self._loadingCounter = max(self._loadingCounter - 1, 0)
        }
    }

    public var isLoading: Bool {
        get {
            _loadingCounter > 0
        }
        set {

        }
    }

    public let clientConfig: ClientConfig

    public init(clientConfig: ClientConfig) {
        self.clientConfig = clientConfig
    }

    private enum APIRoute {
        case register
        case createToken
        case subscribe(userId: String, groupId: String)
        case unsubscribe(userId: String, groupId: String)
        case syncId(userId: String)
        case delete(userId: String)
        case addDevice(userId: String)
        case removeDevice(userId: String, deviceId: String)
        case usersInGroup(groupId: String)
        case usersSyncId(callerId: String, targetId: String)

        func url(config: ClientConfig) -> URL {
            switch self {
            case .register:
                return config.urlAppendingPathComponent("users")
            case let .subscribe(userId, groupId), let .unsubscribe(userId, groupId):
                return config.urlAppendingPathComponent("groups/\(groupId)/subscribers/\(userId)")
            case .createToken:
                return config.urlAppendingPathComponent("users/tokens")
            case .delete(let id):
                return config.urlAppendingPathComponent("users/\(id)")
            case .syncId(let id):
                return config.urlAppendingPathComponent("users/\(id)/sync")
            case .addDevice(let id):
                return config.urlAppendingPathComponent("users/\(id)/devices")
            case let .removeDevice(userId, deviceId):
                return config.urlAppendingPathComponent("users/\(userId)/devices/\(deviceId)")
            case .usersInGroup(let id):
                return config.urlAppendingPathComponent("groups/\(id)/subscribers")
            case let .usersSyncId(callerId, targetId):
                return config.urlAppendingPathComponent("public-users/\(callerId)/sync-id/\(targetId)")
            }
        }

        var method: Method {
            switch self {
            case .register, .createToken, .addDevice, .subscribe:
                return .post
            case .syncId, .usersInGroup, .usersSyncId:
                return .get
            case .delete, .removeDevice, .unsubscribe:
                return .delete
            }
        }

        enum Method: String {
            case post = "POST"
            case get = "GET"
            case delete = "DELETE"
        }
    }

    private func createRequest(type: APIRoute, token: String? = nil) -> URLRequest {
        let url = type.url(config: clientConfig)
        var request = URLRequest(url: url)
        request.setJSONBodyType()
        request.httpMethod = type.method.rawValue
        if let token = token {
            request.setBearer(token: token)
        }
        return request
    }

    private func createRequest<T: Encodable>(type: APIRoute, body: T, token: String? = nil) -> URLRequest {
        var request = createRequest(type: type, token: token)
        let body = try! JSONEncoder().encode(body)
        request.httpBody = body
        return request
    }

    open func wrappedInLoading<T: Publisher>(_ closure: () throws -> T) rethrows -> AnyPublisher<T.Output, T.Failure> {
        let publisher = try closure()
        return wrapLoading(publisher: publisher)
    }

    open func wrapLoading<T: Publisher>(publisher: T) -> AnyPublisher<T.Output, T.Failure> {
        incLoading()
        var decreased = false
        func dec() {
            guard !decreased else { return }
            decreased = true
            self.decLoading()
        }
        return publisher.handleEvents(receiveCompletion: { _ in dec() }, receiveCancel: dec).eraseToAnyPublisher()
    }

    open func login<User: Encodable>(user: User, idKeyPath: KeyPath<User, String>) -> AnyPublisher<SyncIDTokenResponseBody, Error> {

        let tokenTask = createToken(for: user)

        let task = tokenTask.flatMap { token in
            self.getSyncId(userId: user[keyPath: idKeyPath], token: token).map {
                SyncIDTokenResponseBody(syncId: $0, token: token)
            }
        }
        return wrapLoading(publisher: task)
    }


    open func createToken<TokenUser: Encodable>(for user: TokenUser) -> AnyPublisher<String, Error> {
        let request = createRequest(type: .createToken, body: user)

        let task = request.publisher(expectedBodyType: TokenResponseBody.self).map(\.token)
        return wrapLoading(publisher: task)
    }

    open func register<User: Encodable>(user: User) -> AnyPublisher<SyncIDTokenResponseBody, Error> {

        let request = createRequest(type: .register, body: user)

        let req = request.publisher(expectedBodyType: SyncIDTokenResponseBody.self)
        let task = wrapLoading(publisher: req)

        return task
    }

    open func deleteUser(withId id: String, token: String) -> AnyPublisher<Void, Error> {
        let request = createRequest(type: .delete(userId: id), token: token)
        let task = request.publisher().map { _ in () }
        return wrapLoading(publisher: task)
    }

    open func getSyncId(userId: String, token: String) -> AnyPublisher<String, Error> {
        let request = createRequest(type: .syncId(userId: userId), token: token)
        let task = request.publisher(expectedBodyType: SyncIDResponseBody.self).map(\.syncId)
        return wrapLoading(publisher: task)
    }

    open func addDevice<Device: Encodable>(userId id: String, device: Device, token: String) -> AnyPublisher<Void, Error> {
        let request = createRequest(type: .addDevice(userId: id), body: device, token: token)
        let task = request.publisher().map { _ in () }
        return wrapLoading(publisher: task)
    }
    open func addDevice(userId id: String, deviceId: String, token: String) -> AnyPublisher<Void, Error> {
        let request = createRequest(type: .removeDevice(userId: id, deviceId: deviceId), token: token)
        let task = request.publisher().map { _ in () }
        return wrapLoading(publisher: task)
    }

    open func getUsers<User: Decodable>(groupId: String, token: String, userType: User.Type = User.self) -> AnyPublisher<[User], Error> {
        let request = createRequest(type: .usersInGroup(groupId: groupId), token: token)
        let task = request.publisher(expectedBodyType: [User].self)
        return wrapLoading(publisher: task)
    }

    open func syncIdFor(userId: String, interestedUserId: String, token: String) -> AnyPublisher<String, Error> {
        let request = createRequest(type: .usersSyncId(callerId: interestedUserId, targetId: userId), token: token)
        let task = request.publisher(expectedBodyType: SyncIDResponseBody.self).map(\.syncId)
        return wrapLoading(publisher: task)
    }

    open func subscribe(syncId: String, to groupId: String, as role: Role, token: String) -> AnyPublisher<Void, Error> {
        let request = createRequest(type: .subscribe(userId: syncId, groupId: groupId), body: CodableSubscribeBody(role: role), token: token)
        let task = request.publisher(expectedCode: .created).map { _ in }
        return wrapLoading(publisher: task)
    }

    open func unsubscribe(syncId: String, from groupId: String, token: String) -> AnyPublisher<Void, Error> {
        let request = createRequest(type: .unsubscribe(userId: syncId, groupId: groupId), token: token)
        let task = request.publisher(expectedCode: .ok).map { _ in }
        return wrapLoading(publisher: task)
    }
}


struct CodableSubscribeBody: Codable {
    let role: Role
}

extension URLRequest {
    mutating func setJSONBodyType() {
        addValue("application/json", forHTTPHeaderField: "Content-Type")
    }
    mutating func setBearer(token: String) {
        addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
    }

    public struct Code {
        let code: Int
        init(code: Int) {
            self.code = code
        }

        public static let ok = Code(code: 200)
        public static let created = Code(code: 201)
    }

    public func publisher(expectedCode: Code = .ok) -> AnyPublisher<Data, Error> {
        URLSession.shared.dataTaskPublisher(for: self)
            .tryMap { data, response -> Data in
                guard let resp = response as? HTTPURLResponse else {
                    throw URLError(.badServerResponse)
                }
                guard resp.statusCode == expectedCode.code else {
                    throw BadRequest(code: resp.statusCode, body: String(data: data, encoding: .utf8))
                }
                return data
        }.eraseToAnyPublisher()
    }

    public func publisher<T: Decodable, Coder: TopLevelDecoder>(expectedCode: Code = .ok, expectedBodyType: T.Type, decoder: Coder) -> AnyPublisher<T, Error> where Coder.Input == Data {
        publisher(expectedCode: expectedCode).decode(type: T.self, decoder: decoder).eraseToAnyPublisher()
    }

    public func publisher<T: Decodable>(expectedCode: Code = .ok, expectedBodyType: T.Type) -> AnyPublisher<T, Error> {
        publisher(expectedCode: expectedCode).decode(type: T.self, decoder: JSONDecoder()).eraseToAnyPublisher()
    }
}

