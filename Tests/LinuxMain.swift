import XCTest

import MeerkatClientSyncControllerTests

var tests = [XCTestCaseEntry]()
tests += MeerkatClientSyncControllerTests.allTests()
XCTMain(tests)
