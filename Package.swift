// swift-tools-version:5.1

import PackageDescription

let package = Package(
    name: "MeerkatClientSyncController",
    platforms: [
        .macOS("10.15"),
        .iOS("13.0")
    ],
    products: [
        .library(
            name: "MeerkatClientSyncController",
            targets: ["MeerkatClientSyncController"]),
    ],
    dependencies: [
        .package(url: "https://gitlab.com/MeerkatSync/meerkatclientcore", from: "1.0.0"),
    ],
    targets: [
        .target(
            name: "MeerkatClientSyncController",
            dependencies: ["MeerkatClientCore"]),
        .testTarget(
            name: "MeerkatClientSyncControllerTests",
            dependencies: ["MeerkatClientSyncController"]),
    ]
)
